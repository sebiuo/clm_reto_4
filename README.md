# Reto 4


Para desplegar el cluster es necesario tener instalado:
#### 0- Un proyecto creado en GCP
#### 1- Google cloud SDK
#### 2- Terraform
#### 3- Kubectl

## Terrafrom

Dentro de la carpeta contenida editar el archivo locals.tf 
donde se encuentra el nombre de ID del proyecto y la región donde se 
desplegará.
```
cd terraform/
vim locals.tf
```
También es necesario modificar el archivo account.tf, el nombre de la cuenta luego se necestirará.
```
vim account.tf
```

Ya modificado el archivo, es hora de desplegar el cluster, iniciando terrafrom.
```
terraform init
```
Es importante revisar la configuración, y si se encuentra ya desplegado el cluster ver los
cambios que realizará.
```
terraform plan
```
Todo OK? Se aplica la configuración
```
terraform apply
```

## Kubernetes

Ya con el cluster operativo, se aplican las configuraciones necesarias para ejecutar la aplicación en la nube.

Dentro de la carpeta k8s, para crear el namespace development y desplegar el manifiesto dentro del namespace se ejecuta.
```
cd k8s/
kubectl create -f ns_developer.json
kubectl apply -f deployment.yaml --namespace=development
```
Como es necesario acceder a la app usando https se instala un ingress controller para administrar las peticiones HTTP dentro del cluster,
para realizar esta instalación se utiliza un chart de HELM.
```
helm upgrade --install sebingress ingress-nginx \
  --repo https://kubernetes.github.io/ingress-nginx \
  --namespace ingress-nginx --create-namespace
```
Para usar HTTPS en el cluster se utiliza cert-manager para generar y administrar los certificados SSL, y para un mayor
control se crea el namespace cert-manager
```
kubectl create namespace cert-manager
kubectl apply -f https://github.com/jetstack/cert-manager/releases/download/v1.6.0/cert-manager.yaml
```
Ahora aplicamos la configuración del ingres, se utiliza el archivo ingress.yaml
```
kubectl apply -f ingress.yaml --namespace=development
```
Terminado este proceso se agrega el horizontal pod scaler, donde se encuentran los parametros de replicación.
```
kubectl apply -f has.yaml --namespace=development
```

Terminado estos pasos se puede revisar la aplicación desplegada en:

### https://clm-reto4.sebapi.ga/
