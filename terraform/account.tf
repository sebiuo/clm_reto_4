# Se crea una cuenta de servicio en el proyecto y se le asignan permisos para crear y usar elementos en el GKE

resource "google_service_account" "service_account" {
  account_id   = "sebapi-id"
  display_name = "sebano"
  project = local.project_id 
}

resource "google_project_iam_member" "project" {
  project = local.project_id
  role    = "roles/container.admin"
  member  = "serviceAccount:${google_service_account.service_account.email}"
}



