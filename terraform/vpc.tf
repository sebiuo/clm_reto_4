provider "google" {
  project = local.project_id
  region  = local.region
  zone    = "us-central1-a"
  credentials = file("secret.json")
}

provider "google-beta" {
  project = local.project_id
  region  = local.region
   private_ip_google_access = true
}

# Se carga data, del proyecto reto-clm-101
data "google_project" "reto-clm-101" {
}

# VPC
resource "google_compute_network" "vpc-network-sevops" {
  name                    = "${local.project_id}vpc-network-sevops"
  auto_create_subnetworks = "false"
  project                 = data.google_project.reto-clm-101.number
  # routing_mode            = "REGIONAL"
  # mtu                     = 1500
}

# Subnet
resource "google_compute_subnetwork" "vpc-subnet-sevops" {
  name          = "${local.project_id}-vpc-subnet-sevops"
  region        = local.region
  network       = google_compute_network.vpc-network-sevops.name
  ip_cidr_range = "10.10.0.0/24"
}