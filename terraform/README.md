# Control de version de TERRAFORM en GCP
Provisiona la infrastructura en GKE

Terraform ayuda a desplegar de forma declarativa la infrastructura desplegada,
controla el estado actual del cluster, y es facilmente leible su estrctura.

### 1- Es necesario tener activado en GCP:
    - API kubernetes
    - API IAM
    - Estar autentificado en GCP

### 2- Terraform
Se utiliza terraform para desplegar el cluster de kubernetes en Google Cloud Platform

### 3- VPC 
Se configura la Virtual Private Cloud, y su subred en el archivo vpc.tf

### 4- GKE
Luego se crea el cluster con los parametros definidos en el archivo gke.tf

### 5 LOCALS
Se usa el archivo locals.tf como locals donde se encuentra la variable id de proyecto y la region de despliegue.


