terraform {
  required_providers {
    kubernetes = {
      source = "hashicorp/kubernetes"
      version = "2.6.1"
    }
  }
}

provider "kubernetes" {
  config_path    = "~/.kube/config"
}

resource "kubernetes_role" "s3b-role" {
  metadata {
    name = "s3b-role"
  }
  rule {
    api_groups     = ["","extensions", "apps", "apps/v1", "networking.k8s.io"]
    resources      = ["pods"]
    verbs          = ["get", "list", "watch"]
  }
}

resource "kubernetes_role_binding" "s3b-role-binding" {
  metadata {
    name      = "terraform-s3b-binding"
    namespace = "desarrollo"
  }
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "Role"
    name      = "s3b-role"
  }
  subject {
    kind      = "User"
    name      = "admin"
    api_group = "rbac.authorization.k8s.io"
  }
}