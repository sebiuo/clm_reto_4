variable "gke_num_nodes" {
  default     = 1
  description = "number of gke nodes" # por defecto es 1 nodo en cada zona disponible
}

# GKE cluster
resource "google_container_cluster" "cluster" {
  name     = "${local.project_id}-gke"
  location = local.region
  # Se crea un nodo para poder crear el cluster...
  remove_default_node_pool = true
  initial_node_count       = 1
  network    = google_compute_network.vpc-network-sevops.name
  subnetwork = google_compute_subnetwork.vpc-subnet-sevops.name
}

# Node Pool
resource "google_container_node_pool" "cluster_nodes" {
  name       = "${google_container_cluster.cluster.name}-nodes"
  location   = local.region
  cluster    = google_container_cluster.cluster.name
  node_count = var.gke_num_nodes
  autoscaling {
     min_node_count = "1"
     max_node_count = "5"
  }

  node_config { 
    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = local.project_id
    }

    # preemptible  = true
    # tipo de nodo 2cpu....
    machine_type = "n1-standard-1"
    disk_size_gb = 100
    tags         = ["gke-node", "${local.project_id}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}